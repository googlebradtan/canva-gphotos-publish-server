// Copyright 2023 Google LLC

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const express = require('express');
const { OAuth2Client } = require('google-auth-library');
const googlePhotos = require('googlephotos');
const bodyParser = require('body-parser');
const { v4: uuidv4 } = require('uuid');
const request = require('request')
const fs = require('fs')
var fileExtension = require('file-extension');
const querystring = require ("querystring");
var tempState;
var tempUser;
const app = express();
app.use(bodyParser.json());
var Datastore = require('nedb')
  , db = new Datastore({ filename: './users.db', autoload: true });

const GOOGLE_CLIENT_ID = "REPLACE_WITH_YOUR_ID"
const GOOGLE_CLIENT_SECRET = "REPLACE_WITH_YOUR_KEY"

const clientId = GOOGLE_CLIENT_ID;
const clientSecret = GOOGLE_CLIENT_SECRET;
const redirectUri = 'YOUR_CANVA_REDIRECT_URI/auth/callback';

const oauth2Client = new OAuth2Client(clientId, clientSecret, redirectUri);


function checkUserID(userID){
  db.find({ user: userID }, function (err, docs) {
    if (docs){
      return true
    }
    return false
  });
}

app.post("/configuration", async (request, response) => {
  
    // The user is logged-in
    if (checkUserID(request.body.user)) {
            response.send({
                type: "SUCCESS",
                labels: ["PUBLISH"],
              });
              return;
    }
    
  
    // The user is not logged-in
    response.send({
      type: "ERROR",
      errorCode: "CONFIGURATION_REQUIRED",
    });
  });


app.get('/auth', (req, res) => {
  tempState = req.query.state
  tempUser = req.query.user
    const url = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: ['https://www.googleapis.com/auth/photoslibrary']
    });
    res.redirect(url);
});


app.get('/auth/callback', (req, res) => {

    const code = req.query.code
    oauth2Client.getToken(code, (err, token) => {
        if (err) {
            console.log(`Error getting token: ${err}`);
            res.send('Error getting token');
        } else {
            oauth2Client.setCredentials(token);
            accesstoken = token.access_token

            if (checkUserID(tempUser)){
                const newDoc = { user: tempUser, authToken: accesstoken };

                db.insert(newDoc, (err, insertedDoc) => {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log(`Successfully inserted document: ${insertedDoc}`);
                  }
                });
            }
            else{
                     db.update({ user: tempUser}, {user: tempUser, authToken: accesstoken}, { upsert: true }, function (err, newDoc) {   // Callback is optional
                      console.log("upserted00")
                      console.log(newDoc)
               });
            }

    // Create query parameters for redirect back to Canva
    const params = querystring.stringify({
      success: true,
      state: tempState,
    });

    // Redirect back to Canva
    res.redirect(302, `https://canva.com/apps/configured?${params}`);

        }
    });
});

function deleteLocalFile(filePath) {
    // Check if the file exists
    if (fs.existsSync(filePath)) {
        // Delete the file
        fs.unlinkSync(filePath);
        console.log(`File ${filePath} deleted successfully.`);
    } else {
        console.log(`File ${filePath} does not exist.`);
    }
}


function uploadImage(filePath, authToken){


  const imagetoUpload = fs.createReadStream(filePath);
  console.log(filePath)
  var mimeType = fileExtension(filePath)



  request({
      headers: {
          'Content-Type': 'application/octet-stream',
          'Authorization': 'Bearer '+authToken,
          'X-Goog-Upload-Content-Type': 'image/'+mimeType,
          'X-Goog-Upload-Protocol': 'raw'
      },
      uri: 'https://photoslibrary.googleapis.com/v1/uploads',
      body: imagetoUpload,
      method: 'POST'
    }, function (err, result, body) {
      photoToken = result.body
      console.log(photoToken)

      // upload with new token
      request({
          headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+authToken,

          },
          uri: 'https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate',
          body:       {
              "newMediaItems": [
                {
                  "description": "Uploaded from Canva through Google Cloud",
                  "simpleMediaItem": {
                    "fileName": filePath,
                    "uploadToken": photoToken
                  }
                }
              ]
            },
            json: true,
          method: 'POST'
        }, function (err, result, body) {
          deleteLocalFile(filePath)
          return(result)
  
        });
    });
}

app.post('/publish/resources/upload', (req, res) => {

    var uploadToken;
    const assets = req.body.assets;

    var userObject;
    console.log("USER HERE")
    console.log(req.body.user)

    db.find({ user: req.body.user }, function (err, docs) {
      console.log("DOCS HERE")
      console.log(docs)
      imgPathList = []

    for (i=0; i<req.body.assets.length; i++){

      const fileType = req.body.assets[i].url.split('.').pop();
      const fileTypeWithoutQn = fileType.split('?')[0];
      const uuidWithoutDash = uuidv4().replace(/-/g, '');
      const shortUUID = uuidWithoutDash.substring(0, 10);

      const fileName = `${shortUUID.toString()}.${fileTypeWithoutQn}`;
      const filePath = `temp/${fileName}`;

      request(req.body.assets[i].url)
      .pipe(fs.createWriteStream(filePath)) // download the image and save it to the temp folder
      .on("close", () => {
        fs.readFile(filePath, (err, data) => {
          if (err) throw err;
          
          console.log(data)
          uploadImage(filePath, docs[0].authToken)
          res.send({
            type: "SUCCESS",
            url: "https://photos.google.com"
          });
  
        });
      });  
    }
  });
});



app.listen(process.env.PORT || 3000, () => {
    console.log('Server listening on port 3000');
});
