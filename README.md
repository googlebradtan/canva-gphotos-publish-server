```
Copyright 2023 Google. This software is provided as-is, without warranty or representation for any use or purpose. Your use of it is subject to your agreement with Google. 
```

## Setup

### Install dependencies:

    npm install

### Update Keys:

Update below variables in `server.js`

- GOOGLE_CLIENT_ID
- GOOGLE_CLIENT_SECRET
- YOUR_CANVA_REDIRECT_URI in `redirectUri` variable